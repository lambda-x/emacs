;;;; -*- lexical-binding: t -*-

(let ((default-directory "~/emacs"))
  (normal-top-level-add-subdirs-to-load-path))


(require 'a-defaults)
(require 'a-minimal)
(require 'a-packages)
(require 'a-languages)
(require 'a-org)
(require 'a-org-roam)
(require 'a-mu4e)
(require 'a-appearance)
(require 'a-evil)
