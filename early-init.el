;;;; -*- lexical-binding: t -*-

;; (setq native-comp-speed 2
;;       native-comp-deferred-compilation t)
;; (when (boundp 'native-comp-eln-load-path)
;;   (setcar native-comp-eln-load-path
;;           (expand-file-name ".local/temp/cache/eln-cache/" user-emacs-directory)))

;; Silence nativecomp warnings popping up on 28.0.50
(setq native-comp-async-report-warnings-errors nil)

(setq frame-inhibit-implied-resize t)

;; max memory available for gc on startup
(defvar me/gc-cons-threshold 16777216)

(setq gc-cons-threshold most-positive-fixnum
      gc-cons-percentage 0.6)

;; (add-hook 'emacs-startup-hook
;;           (lambda ()
;;             (setq gc-cons-threshold me/gc-cons-threshold
;;                   gc-cons-percentage 0.1)))

;; The default is 800 kilobytes.  Measured in bytes.
;; (setq gc-cons-threshold (* 50 1000 1000))

(defun efs/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                     (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook #'efs/display-startup-time)

;; max memory available for gc when opening minibuffer
(defun me/defer-garbage-collection-h ()
  (setq gc-cons-threshold most-positive-fixnum))

(defun me/restore-garbage-collection-h ()
  ;; Defer it so that commands launched immediately after will enjoy the
  ;; benefits.
  (run-at-time
   1 nil (lambda () (setq gc-cons-threshold me/gc-cons-threshold))))

(add-hook 'minibuffer-setup-hook #'me/defer-garbage-collection-h)
(add-hook 'minibuffer-exit-hook #'me/restore-garbage-collection-h)

(setq garbage-collection-messages t)

(defvar me/-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)
(add-hook 'emacs-startup-hook
          (lambda ()
            (setq file-name-handler-alist me/-file-name-handler-alist)))

;; Hard code the path to libgccjit. See:
;; https://github.com/d12frosted/homebrew-emacs-plus/issues/323
;; (setenv "LIBRARY_PATH" "/usr/local/Cellar/gcc/11.2.0_3/lib/gcc/11:/usr/local/opt/gcc/lib/gcc/11.2.0_3/gcc/x86_64-apple-darwin20/11")
;; Not necessary for Emacs from Nix.
;; (setenv "LIBRARY_PATH" "/usr/local/Cellar/libgccjit/11.2.0_1")

(setq inhibit-compacting-font-caches t)

(when (boundp 'read-process-output-max)
  ;; 1MB in bytes, default 4096 bytes
  (setq read-process-output-max 1048576))

(unless (eq window-system 'ns)
  (menu-bar-mode -1))

(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))

(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))

(when (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode -1))

(setq initial-major-mode 'fundamental-mode)
(setq default-frame-alist
      (append (list
               '(internal-border-width . 10)
               '(left-fringe    . 10)
               '(right-fringe   . 0)
               '(tool-bar-lines . 0)
               '(menu-bar-lines . 0)
               '(vertical-scroll-bars . nil)
               '(horizontal-scroll-bars . nil)
               '(height . 65)
               '(width . 100)
               )))



(provide 'early-init)
