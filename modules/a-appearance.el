;; Themes are listed in chronological order of my discovery

(use-package modus-themes)
;; (use-package modus-themes
;;   :init
;;   (defun modus-themes-load-operandi ()
;;     "Load `modus-operandi' and disable `modus-vivendi'.
;;      Also run `modus-themes-after-load-theme-hook'."
;;     (disable-theme 'modus-vivendi)
;;     (load-theme 'modus-operandi t)
;;     (run-hooks 'modus-themes-after-load-theme-hook))
;;   (setq modus-themes-subtle-line-numbers t)
;;   (setq modus-themes-org-blocks 'tinted-background)
;;   (setq modus-themes-completions 'opinionated)
;;   (setq modus-themes-prompts 'intense-accented)
;;   (setq modus-themes-headings '((1 . line)
;;                                 (2 . line)
;;                                 (3 . no-bold)))
;;   (setq modus-themes-mode-line 'moody)
;;   (setq modus-themes-scale-headings t)
;;   (setq modus-themes-scale-1 1
;;         modus-themes-scale-2 1.1
;;         modus-themes-scale-3 1.15
;;         modus-themes-scale-4 1.5
;;         modus-themes-scale-5 1.5)
;;   (setq modus-themes-variable-pitch-ui t)
;;   (setq modus-themes-variable-pitch-headings t)
;;   (setq modus-themes-slanted-constructs t)
;;   (setq modus-themes-bold-constructs nil)
;;   :config
;;   (set-cursor-color "#2e8b57")
;;   :commands (consult-theme))

(use-package parchment-theme
  :config
  (set-cursor-color "#47709e")
  :commands (consult-theme))

(use-package plan9-theme
  :config
  (setq org-todo-keyword-faces
        '(("TODO" . (:background "#FCF8D2" :foreground "#6d6832"))
          ("NOTE" . (:background "#676767" :foreground "#efefef"))
          ("DONE" . (:background "#d1ffed" :foreground "#598f6e"))))
  :commands (consult-theme)) 

(use-package acme-theme
  :defer
  :commands (consult-theme))

(use-package humanoid-themes
  :defer
  :commands (consult-theme))

(use-package material-theme
  :defer
  :commands (consult-theme))

(use-package doom-themes
  :defer
  :config
  (setq doom-themes-enable-bold t)
  (setq doom-themes-enable-italic t)
  :commands (consult-theme))

(use-package tango-plus-theme
  :defer
  :commands (consult-theme))

(use-package gruvbox-theme
  :defer
  :commands (consult-theme))

(use-package almost-mono-themes
  :disabled)

(use-package ayu-theme
  :defer
  :commands (consult-theme))

(use-package mood-line
  :commands (mood-line-mode))

(use-package minions
  :defer 1
  :config
  (minions-mode 1))

(use-package doom-modeline
  :custom
  (doom-modeline-height 15)
  :commands (doom-modeline-mode))

(use-package bespoke-themes
  :straight (:host github :repo "mclear-tools/bespoke-themes" :branch "main")
  :config
  (setq bespoke-set-italic-comments t)
  (setq bespoke-set-italic-keywords t)
  (setq bespoke-set-theme 'light)
  :commands (consult-theme))

(use-package kaolin-themes
  :defer
  :config
  (setq kaolin-themes-italic-comments t)
  :commands (consult-theme))

(use-package darcula-theme
  :commands (consult-theme))

(use-package one-themes
  :commands (consult-theme))

(use-package ample-theme
  :commands (consult-theme))

(use-package sublime-themes
  :commands (consult-theme))

(use-package darktooth-theme
  :commands (consult-theme))

(require 'modus-themes)
(load-theme 'modus-operandi-tinted :no-confirm)

;; Not needed with bespoke theme
(doom-modeline-mode)

(set-cursor-color "#47709e")

(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

(defun set-my-fonts ()
  (message "Setting faces!")
  (set-face-attribute 'default nil
                      :font "JetBrains Mono-13" :weight 'normal)
  (set-face-attribute 'fixed-pitch nil
                      :font "JetBrains Mono-13")
  (set-face-attribute 'variable-pitch nil
                      :font "JetBrains Mono-13"))

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (with-selected-frame frame
                  (set-my-fonts))))
  (set-my-fonts))

;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))

(provide 'a-appearance)
