;; These are intended to be used with an 80% keyboard

;; (global-set-key (kbd "C-<left>") #'move-beginning-of-line)
(global-set-key (kbd "C-a") #'crux-move-beginning-of-line)
(global-set-key (kbd "C-<left>") #'crux-move-beginning-of-line)
(global-set-key (kbd "C-<right>") #'move-end-of-line)
(global-set-key (kbd "C-<delete>") #'kill-line)
(global-set-key (kbd "M-<delete>") #'kill-word)
(global-set-key (kbd "C-M-<delete>") #'sp-kill-sexp)

;; Function key bindings
(global-set-key (kbd "<f2>") #'execute-extended-command)
(global-set-key (kbd "<f5>") #'switch-buffer)
(global-set-key (kbd "<f6>") #'bookmark-jump)

(global-set-key (kbd "<f9>") #'projectile-find-file)
(global-set-key (kbd "<f10>") #'projectile-switch-to-buffer)
(global-set-key (kbd "<f12>") #'projectile-switch-project)

(global-set-key (kbd "<f8>") #'avy-goto-char-2)
(global-set-key (kbd "<f13>") #'avy-goto-char-2)
(global-set-key (kbd "<f15>") #'save-buffer)

(define-key (current-global-map) [remap switch-buffer] #'consult-buffer)
(define-key (current-global-map) [remap bookmark-jump] #'consult-bookmark)

(use-package emacs
  ;; Space Cadet Bindings
  ;; These are my standard bindings
  :config
  (setq mac-command-modifier 'control)
  (setq mac-option-modifier 'meta)
  (setq mac-control-modifier 'super)
  (setq isearch-allow-motion t))

(use-package emacs
  ;; Standard keybindings
  :disabled
  :config
  (setq mac-command-modifier 'meta)
  (setq mac-option-modifier 'option)
  (setq mac-control-modifier 'control))

(use-package emacs
  :disabled
  :config
  (setq mac-command-modifier 'super)
  (setq mac-option-modifier 'meta)
  (setq mac-control-modifier 'control))

(provide 'a-bindings)
