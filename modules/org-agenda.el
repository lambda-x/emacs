(use-package org-agenda
  :ensure nil
  :after org
  :config
  (setq org-agenda-confirm-kill t)
  :bind (("C-c a" . org-agenda)))

(use-package org-super-agenda
  :after org-agenda
  :init
  (setq org-super-agenda-header-map (make-sparse-keymap))
  :config
  (setq org-super-agenda-groups '((:name "Today"
                                         :time-grid t
                                         :scheduled today)
                                  (:name "Due Today"
                                         :deadline today)
                                  (:name "Overdue"
                                         :deadline past)
                                  (:name "Due Soon"
                                         :deadline future)))
  (org-super-agenda-mode))
