;;;; -*- lexical-binding: t -*-

(use-package evil
  :defer .1
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-fine-undo t)
  (setq evil-search-module 'evil-search)
  (setq evil-want-Y-yank-to-eol t)
  (setq evil-undo-system 'undo-redo) ; Emacs 28
  :config
  (setq mac-command-modifier 'control)
  (setq mac-option-modifier 'meta)
  (setq mac-control-modifier 'super)
  ;; (setq isearch-allow-motion t)
  (evil-set-leader 'normal (kbd "SPC"))
  (setq evil-magic 'very-magic)
  (run-with-idle-timer 15 t 'evil-normal-state)
  (define-key evil-normal-state-map "s"  'evil-avy-goto-char-2-below)
  (define-key evil-normal-state-map "S"  'evil-avy-goto-char-2-above)
  (define-key evil-normal-state-map "gsj"  'evil-avy-goto-line-below)
  (define-key evil-normal-state-map "gsk"  'evil-avy-goto-line-above)
  (define-key evil-operator-state-map "z"  'evil-avy-goto-char-2-below)
  (define-key evil-operator-state-map "Z"  'evil-avy-goto-char-2-above)
  (define-key evil-normal-state-map (kbd "<leader> fs")  'save-buffer)
  (define-key evil-normal-state-map (kbd "<leader> ff")  'find-file)
  (define-key evil-normal-state-map (kbd "<leader> b")  'consult-buffer)
  (define-key evil-normal-state-map (kbd "<leader> d")  'dired)
  (define-key evil-normal-state-map ",pf" 'projectile-find-file)
  (define-key evil-normal-state-map ",pp" 'projectile-switch-project)
  (define-key evil-normal-state-map ",pb" 'projectile-switch-to-buffer)
  (define-key evil-normal-state-map ",wl" 'evil-window-right)
  (define-key evil-normal-state-map ",wh" 'evil-window-left)
  (define-key evil-normal-state-map ",wj" 'evil-window-down)
  (define-key evil-normal-state-map ",wk" 'evil-window-up)
  (define-key evil-normal-state-map ",wc" 'evil-window-delete)
  (evil-mode 1))

(use-package evil-collection
  :demand t
  :after evil
  :custom
  (setq evil-collection-company-use-tng nil)
  (evil-collection-outline-bind-tab-p nil)
  :config
  (evil-collection-init))

(use-package evil-surround
  :demand t
  :after evil
  :config
  (global-evil-surround-mode 1))

(use-package evil-embrace
  :demand t
  :after evil-surround
  :config
  (setq evil-embrace-show-help-p nil)
  (evil-embrace-enable-evil-surround-integration))

(use-package evil-exchange
  :after evil
  :config
  (evil-exchange-install)
  :commands
  (evil-exchange))

(use-package evil-snipe
  :custom
  (evil-snipe-scope 'visible)
  :config
  (add-hook 'magit-mode-hook 'turn-off-evil-snipe-override-mode)
  (evil-snipe-override-mode +1)
  :commands (evil-snipe-mode))

(use-package evil-org
  :after (org evil)
  :hook ((org-mode . evil-org-mode)
         (org-agenda-mode . evil-org-mode)
         (evil-org-mode . (lambda () (evil-org-set-key-theme
                                      '(navigation
                                        todo
                                        insert
                                        textobjects
                                        additional
                                        calendar)))))
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys)
  :blackout
  :commands (evil-org))

(use-package evil-easymotion
  :config
  (evilem-default-keybindings "gs"))

;; Comment regions using gc MOTION
(use-package evil-commentary
  :demand t
  :after evil
  :config
  (evil-commentary-mode))

;; Align regions using gl or gL plus MOTION CHAR
(use-package evil-lion
  :after evil
  :bind (:map evil-normal-state-map
              ("g l " . evil-lion-left)
              ("g L " . evil-lion-right)
              :map evil-visual-state-map
              ("g l " . evil-lion-left)
              ("g L " . evil-lion-right))
  :config
  (evil-lion-mode))

(use-package tab-jump-out
  :demand t
  :init
  (add-hook 'after-init-hook 'tab-jump-out-mode))

(use-package evil-textobj-tree-sitter
  :after tree-sitter
  :config
  (define-key evil-outer-text-objects-map "a" (evil-textobj-tree-sitter-get-textobj "argument.outer"))
  (define-key evil-inner-text-objects-map "a" (evil-textobj-tree-sitter-get-textobj "argument.inner"))
  (define-key evil-outer-text-objects-map "p" (evil-textobj-tree-sitter-get-textobj "parameter.outer"))
  (define-key evil-inner-text-objects-map "p" (evil-textobj-tree-sitter-get-textobj "parameter.inner"))
  (define-key evil-outer-text-objects-map "f" (evil-textobj-tree-sitter-get-textobj "function.outer"))
  (define-key evil-inner-text-objects-map "f" (evil-textobj-tree-sitter-get-textobj "function.inner"))
  (define-key evil-outer-text-objects-map "c" (evil-textobj-tree-sitter-get-textobj "class.outer"))
  (define-key evil-inner-text-objects-map "c" (evil-textobj-tree-sitter-get-textobj "class.inner")))

(defun my-center-line (&rest _)
  (evil-scroll-line-to-center nil))

(advice-add 'evil-search-next :after #'my-center-line)

(provide 'a-evil)
