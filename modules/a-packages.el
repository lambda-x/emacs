;;;; -*- lexical-binding: t -*-
(use-package abbrev-mode
  :straight nil
  :hook org-mode
  :init
  (define-abbrev-table 'global-abbrev-table '(("ualpha" "α")
                                              ("ubeta"  "β")
                                              ("ugamma" "γ")
                                              ("ulambda" "λ")
                                              ("uomega" "ω")
                                              ("uthereis" "∃")
                                              ("uall" "∀")
                                              ("uthen" "→")
                                              ("uiff" "⟺")
                                              ("uor" "∨")
                                              ("uand" "∧")
                                              ("uqed" "∴")
                                              ("naive" "naïve")
                                              ("Naive" "Naïve")
                                              ("Noe" "Noë")))
  :blackout t)

(use-package amx
  :commands (amx))

(use-package key-chord
  :defer t)

(use-package use-package-chords
  :config (key-chord-mode 1))

(use-package avy
  :bind (("C-." . #'avy-goto-char-2)
         ("<ESC>j" . #'avy-goto-char-2))
  :config
  ;; (global-set-key (kbd "<tab>n") #'avy-goto-char-2)
  (key-chord-define-global "fj" #'avy-goto-char-2)
  (avy-setup-default))

(use-package beacon
  :defer 0
  :custom
  (beacon-push-mark 10)
  :blackout)

(use-package company
  :defer 1
  :hook (
         ;; (prog-mode . company-mode)
         (lsp-mode . company-mode))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0)
  (company-selection-wrap-around t)
  :config
  (setq global-company-mode nil)
  :blackout)

(use-package company-quickhelp
  :disabled
  :defines company-quickhelp-delay
  :bind (:map company-active-map
              ("C-c h" . company-quickhelp-manual-begin))
  :hook (global-company-mode . company-quickhelp-mode)
  :custom (company-quickhelp-delay 0.8))

(use-package company-box
  :disabled
  :after company
  :hook (company-mode . company-box-mode)
  :blackout)

(use-package company-ghci
  :disabled
  :config
  (push 'company-ghci company-backends)
  :hook ((haskell-mode . company-mode)
         (haskell-interactive-mode . company-mode)))

(use-package company-nixos-options
  :disabled
  :defer
  :config
  (add-to-list 'company-backends 'company-nixos-options))

(use-package company-posframe
  :disabled
  :hook (company-mode . company-posframe-mode))

(use-package corfu
  :commands (corfu-mode))

(use-package crux
  :defer 1
  ;; :commands (crux-beginning-of-line)
  )

(use-package deadgrep
  :general
  ("M-s g" #'deadgrep))

;; Feature `delsel' provides an alternative behavior for certain
;; actions when you have a selection active. Namely: if you start
;; typing when you have something selected, then the selection will be
;; deleted; and if you press DEL while you have something selected, it
;; will be deleted rather than killed. (Otherwise, in both cases the
;; selection is deselected and the normal function of the key is
;; performed.)
(use-package delsel
  :straight nil
  :defer 0
  :config
  (delete-selection-mode +1))

(use-package dired
  :straight nil
  :hook (dired-mode . dired-hide-details-mode)
  :config
  (when (string= system-type "darwin")
    (setq dired-use-ls-dired nil))
  (setq global-auto-revert-non-file-buffers t)
  (setq auto-revert-verbose nil))

;; See this discussion:
;; https://github.com/wbolster/emacs-direnv/issues/17 
(use-package direnv
  :defer 2
  :hook (before-hack-local-variables  . direnv--maybe-update-environment)
  :hook (flycheck-before-syntax-check . direnv--maybe-update-environment)
  :init
  (setq direnv--executable (concat default-directory "/.nix-profile/bin/direnv"))
  :config
  (setq direnv-always-show-summary t)
  (direnv-mode))

(use-package eros
  :defer 1)

(use-package eshell-syntax-highlighting
  :after esh-mode
  :config
  (eshell-syntax-highlighting-global-mode +1))

(use-package exec-path-from-shell
  :disabled
  :defer 0
  :if (memq window-system '(mac ns x))
  :init
  (exec-path-from-shell-initialize))
  ;; (add-to-list 'exec-path "/usr/local/bin"))

(use-package expand-region
  :bind
  ("C-=" . er/expand-region)
  :commands (er/expand-region))

(use-package gnus
  :straight nil
  :disabled
  :custom
  (gnus-select-method '(nnimap "gmail"
                               (nnimap-address "imap.gmail.com")))
  (message-send-mail-function 'smtp-send-it)
  ;; (gnus-message-archive-group )
  :commands (gnus))

(use-package helpful
  :defer 0
  :bind
  ([remap describe-function] . helpful-callable)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-key] . helpful-key))

(use-package hydra
  :defer t)


(use-package use-package-hydra
  :defer t)

(use-package imenu
  :straight nil
  :custom
  (org-imenu-depth 5)
  (imenu-use-markers t)
  (imenu-auto-rescan t)
  (imenu-auto-rescan-maxout 600000)
  (imenu-max-item-length 100)
  (imenu-use-popup-menu nil)
  (imenu-eager-completion-buffer t)
  (imenu-space-replacement " ")
  (imenu-level-separator "/")
  :bind  ("C-c i" . consult-imenu))

(use-package keycast
  :commands (keycast-mode))

(use-package flimenu
  :disabled
  :defer 0
  :after imenu
  :custom
  (flimenu-imenu-separator " :: ")
  :config
  (flimenu-global-mode 1))

(use-package flycheck
  :commands (flycheck-mode))

(use-package fzf
  :custom
  (fzf/directory-start "~/")
  :commands (fzf-find-file))

(unless (boundp 'org-latex-classes)
   (setq org-latex-classes nil))

(use-package logview
  :commands (logview-mode))

(use-package magit
  :bind ("C-x g" . magit-status))

(use-package nix-buffer
  :disabled t)

(use-package nix-mode
  :mode "\\.nix\\'")

(use-package objed
  :commands (objed-mode))

(use-package olivetti
  :custom
  (olivetti-body-width 80)
  :commands (olivetti-mode))

(use-package projectile
  :defer 1
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :custom
  (projectile-project-search-path '("~/dots/" "~/source/" "~/source/js/" "~/source/emacs-configs/"))
  :config
  (projectile-global-mode t))

(use-package rg
  :config
  (rg-enable-menu)
  :bind
  (("C-c s" . rg-menu))
  :commands (rg rg-project rg-dwim))

(use-package smartparens
  :defer 2
  :config
  ;; Load the default pair definitions for Smartparens.
  (require 'smartparens-config)
  (global-set-key (kbd "s-i") #'sp-change-enclosing)
  (sp-local-pair 'org-mode "`" "'")
  ;; (sp-pair "'" nil :actions :rem)
  (setq sp-highlight-pair-overlay nil)
  (global-set-key (kbd "C-)") 'sp-forward-slurp-sexp)
  (global-set-key (kbd "C-(") 'sp-backward-slurp-sexp)
  (global-set-key (kbd "C-}") 'sp-forward-barf-sexp)
  (global-set-key (kbd "C-{") 'sp-backward-slurp-sexp)
  (global-set-key (kbd "C-M-f") 'sp-forward-sexp)
  (global-set-key (kbd "C-M-b") 'sp-backward-sexp)
  (global-set-key (kbd "C-M-k") 'sp-kill-sexp)
  (global-set-key (kbd "<C-M-backspace>") 'sp-backward-kill-sexp)

  ;; From radian.el:
  ;; Don't disable autoskip when point moves backwards. (This lets you
  ;; open a sexp, type some things, delete some things, etc., and then
  ;; type over the closing delimiter as long as you didn't leave the
  ;; sexp entirely.)

  (setq sp-cancel-autoskip-on-backward-movement nil)
  (smartparens-global-mode +1)
  ;; :commands (smartparens-mode)
  :blackout)

;; found this in Ross A. Baker's config on Fri May 20 2022
(use-package string-inflection
  :commands (string-inflection-toggle)
  :config
  (global-set-key (kbd "s-g") #'string-inflection-toggle))

(use-package subword
  :straight nil
  :defer 0
  :config
  (global-subword-mode +1)
  :blackout)

(use-package powerthesaurus
  :bind (("s-t" . powerthesaurus-lookup-word-dwim)
         ("M-s-t" . powerthesaurus-lookup-word))
  :commands (powerthesaurus-lookup-word-dwim))

(use-package treemacs
  :config
  (setq treemacs-width-is-initially-locked nil)
  (with-eval-after-load 'treemacs
  (define-key treemacs-mode-map [mouse-1] #'treemacs-single-click-expand-action))
)

(use-package vterm
  :defer
  :config
  (setq vterm-timer-delay 0.001))

(use-package which-key
  :config

  ;; We configure it so that `which-key' is triggered by typing C-h
  ;; during a key sequence (the usual way to show bindings). See
  ;; <https://github.com/justbur/emacs-which-key#manual-activation>.
  ;; (setq which-key-show-early-on-C-h t)
  ;;(setq which-key-idle-delay most-positive-fixnum)
  (setq which-key-idle-delay 0.3)
  ;; (setq which-key-idle-secondary-delay 1)
  :commands (which-key-mode)
  :blackout t)

(use-package whitespace
  :straight nil
  :custom
  (whitespace-line-column 80)
  (whitespace-style '(face lines-tail))
  :blackout)

(use-package wc-mode
  :commands (count-words))

(use-package wordnut
  :bind (("s-w" . wordnut-search))
  :commands (wordnut-search wordnut-lookup-current-word))

(use-package writegood
  :commands (writegood-mode))

(provide 'a-packages)
