;;;; -*- lexical-binding: t -*-
(setq use-package-enable-imenu-support t)
(setq straight-repository-branch "develop")
(defvar bootstrap-version)
(let ((bootstrap-file
 (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
(bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
  (url-retrieve-synchronously
   "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
   'silent 'inhibit-cookies)
(goto-char (point-max))
(eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)
(setq use-package-verbose t)
(setq use-package-always-defer t)

;; not working right now
;; (use-package benchmark-init
;;   :config
;;   (add-hook 'after-init-hook 'benchmark-init/deactivate))

;; When set to nil, the frame leaves a gap on MacOS.
(setq frame-resize-pixelwise t)

(use-package gcmh
  :init
  (gcmh-mode 1)
  ;; :blackout t
  )

(use-package blackout
  ;; :straight (:host github :repo "raxod502/blackout")
  :demand)

(use-package exec-path-from-shell
  :config
  (exec-path-from-shell-initialize))

(setq default-directory "~/")

(use-package emacs
  :straight nil
  :init
  (global-auto-revert-mode t)
  (add-to-list 'load-path "~/.config/emacs/lisp")
  (add-to-list 'load-path (concat default-directory "/.nix-profile/share/emacs/site-lisp/mu4e"))
  :custom
  (inhibit-startup-screen t)
  :config
  (setq-default delete-by-moving-to-trash t)
  (setq undo-limit 80000000)
  :bind
  ("<C-M-backspace>" . backward-kill-sexp)
  ("M-n" . forward-paragraph)
  ("M-p" . backward-paragraph))

(use-package undo-tree
  :defer 0
  :bind (;; By default, `undo' (and by extension `undo-tree-undo') is
         ;; bound to C-_ and C-/, and `undo-tree-redo' is bound to
         ;; M-_. It's logical to also bind M-/ to `undo-tree-redo'.
         ;; This overrides the default binding of M-/, which is to
         ;; `dabbrev-expand'.
         :map undo-tree-map
         ("M-/" . #'undo-tree-redo))
  :config
  (global-undo-tree-mode +1)
  (setq undo-tree-visualizer-timestamps t)
  :blackout)

(use-package bookmark
  :straight nil
  :config
  (setq bookmark-default-file "~/emacs/bookmark.el")
  :commands (bookmark-bmenu-list))

(use-package no-littering
  :demand)

(setq ispell-program-name "aspell")
(setq ispell-personal-dictionary "~/emacs/aspell.en.pws")

(use-package pdf-tools
  :hook (doc-view-mode . pdf-view-mode)
  :config
  (pdf-tools-install)
  (setq pdf-view-use-scaling t
        pdf-view-use-imagemagick nil)
  (setq-default pdf-view-display-size 'fit-page))

(setq fringe-mode 25)

;; (Defvar last-file-name-handler-alist file-name-handler-alist)
;; (setq gc-cons-threshold 402653184 gc-cons-percentage 0.6
;; file-name-handler-alist nil)

;; (add-hook 'after-init-hook #'(lambda() (setq gc-cons-threshold
;;                                16777216 gc-cons-percentage 0.1
;;                                file-name-handler-alist
;;                                last-file-name-handler-alist)))

(setq
   backup-by-copying t      ; don't clobber symlinks
   backup-directory-alist
    '(("." . "~/.saves"))    ; don't litter my fs tree
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t)       ; use versioned backups

;; (add-hook 'after-save-hook 'backup-each-save)

;; Narrowing is disabled by default so as not to confuse beginners.
;; These lines turn narrowing back on.
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-defun 'disabled nil)

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; (add-hook 'before-save-hook 'whitespace-cleanup)

;;(use-package move-text)
;;(move-text-default-bindings)

(when (eq system-type 'darwin)
  ;; Use Spotlight to search with M-x locate
  (setq locate-command "mdfind"))

;; (when (eq system-type 'darwin)
;;   (add-to-list 'default-frame-alist '(ns-transparent-titlebar .
;;   t))
;;   (add-to-list 'default-frame-alist '(ns-appearance . light)))

(setq visual-bell nil
      ring-bell-function 'ignore)

(show-paren-mode 1)

(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(fset 'yes-or-no-p 'y-or-n-p)

(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)
(setq locale-coding-system 'utf-8) ; pretty
(set-terminal-coding-system 'utf-8) ; pretty
(set-keyboard-coding-system 'utf-8) ; pretty
(set-selection-coding-system 'utf-8) ; please
(prefer-coding-system 'utf-8) ; with sugar on top

(setq sentence-end-double-space 'nil)

(setq fill-column 70)

(use-package recentf
  :straight nil
  :defer 1
  :config
  (setq recentf-max-saved-items 500
        recentf-max-menu-items 15
        recentf-auto-cleanup 'never)
  (recentf-mode 1))

(use-package osx-trash
  :init
  (when (eq system-type 'darwin)
    (osx-trash-setup))
  (setq delete-by-moving-to-trash t))

(delete-selection-mode 1)

(setq-default tab-width 2
              tab-always-indent t
              indent-tabs-mode nil)

;;  (setq debug-on-error t)

(setq line-number-mode t)
(setq column-number-mode t)

; (global-hl-line-mode 1)

(set-cursor-color "#708090")
(blink-cursor-mode 0)

(use-package ibuffer
  :straight nil
  :bind
  ("C-x C-b" . ibuffer))

(setq scroll-preserve-screen-position 'always)

(use-package electric-pair
  :straight nil
  :init
  ;; (add-hook 'org-mode-hook (lambda () (setq electric-pair-pairs '(
  ;;                                                                (?\" . ?\")
  ;;                                                                (?\` . ?\')
  ;;                                                                (?\( . ?\))
  ;;                                                                (?\[ . ?\])
  ;;                                                                (?\{ . ?\})
  ;;                                                                (?\/ . ?\/)
  ;;                                                                (?\_ . ?\_)
  ;;                                                                ;; (?\* . ?\*)
  ;;                                                                )))
  (setq electric-pair-inhibit-predicate
        (lambda (c)
          (if (char-equal c ?\<) t (electric-pair-default-inhibit c))))
  ;; (electric-pair-mode)
  )

(defun my-goto-match-beginning ()
  (when (and isearch-forward isearch-other-end)
    (goto-char isearch-other-end)))

(add-hook 'isearch-mode-end-hook 'my-goto-match-beginning)

(defun delete-current-line (arg)
  "Delete (not kill) the current line."
  (interactive "p")
  (save-excursion
    (delete-region
     (progn (forward-visible-line 0) (point))
     (progn (forward-visible-line arg) (point)))))

(defun my/kill-whole-line (a)
  "Kills entire line and places point at first character of the
previous line."
  (interactive "P")
  (kill-whole-line)
  (previous-line)
  (back-to-indentation))

;; (define-key global-map (kbd "<s-k>") 'delete-current-line)

(global-unset-key (kbd "<C-S-backspace>")) ; This allows me to unset the
                                         ; command, which has been
                                         ; giving me a ton of problems

(use-package kboardz
  :straight nil
  :disabled
  :init
  (message "this is now working!")
  (scroll-bar-mode 1)
  (define-key global-map (kbd "<f5>") 'switch-to-buffer)
  (define-key global-map (kbd "<f6>") 'isearch-forward)
  (define-key isearch-mode-map (kbd "<f6>") 'isearch-repeat-forward)
  (define-key global-map (kbd "<f7>") 'isearch-backward)
  (define-key isearch-mode-map (kbd "<f7>") 'isearch-repeat-backward)
  (define-key global-map (kbd "<f8>") 'avy-goto-char-timer))

(use-package keyboard
  :straight nil
  :disabled
  :config
  (define-key global-map (kbd "<kp-0>") 'undo)
  (define-key global-map (kbd "<kp-decimal>") 'save-buffer)

  ;"Insert" key on macOS.
  (define-key global-map (kbd "<help>") 'org-capture) 

  (define-key global-map (kbd "<f9>") 'save-buffer)
  (define-key global-map (kbd "<f10>") 'undo)
  (define-key global-map (kbd "<f16>")'execute-extended-command))

(defun open-next-line (arg)
      "Move to the next line and then opens a line.
    See also `newline-and-indent'."
      (interactive "p")
      (end-of-line)
      
      (open-line arg)

      (forward-line 1)
      (when newline-and-indent
        (indent-according-to-mode)))

(defun open-previous-line (arg)
      "Open a new line before the current one. 
     See also `newline-and-indent'."
      (interactive "p")
      (beginning-of-line)
      (open-line arg)

      (when newline-and-indent
        (indent-according-to-mode)))

(defun my/open-line-above ()
  "Insert a newline above the current line and put point at beginning."
  (interactive)
  (unless (bolp)
    (beginning-of-line))
  (newline)
  (forward-line -1)
  (indent-according-to-mode))


(defun my/open-line-below ()
  "Insert a newline below the current line and put point at beginning."
  (interactive)
  (unless (eolp)
    (end-of-line))
  (newline-and-indent))

(defun my/second-try (a)
  "This one works too."
  (interactive "P")
  (beginning-of-line)
  (open-line 1)
  (indent-according-to-mode))

(global-set-key (kbd "C-o") #'my/open-line-above)

(global-set-key (kbd "M-o") #'my/open-line-below)

(defun ant/kill-a-sentence (&optional ARG)
"Mimics Vim's delete-a-sentence command."
(interactive "p")
(progn
(forward-char 1)
(backward-sentence)
(kill-sentence)))

(defun ant/kill-a-paragraph (arg)
"Mimic Vim's dap command."
(interactive "p")
        (mark-paragraph arg)
        (kill-paragraph 1))

(global-set-key (kbd "s-e") 'ant/kill-a-sentence)
(global-set-key (kbd "s-h") 'ant/kill-a-paragraph)

(use-package general)

(require 'general)

(use-package multiple-cursors
  :general
  ("C->"  #'mc/mark-next-like-this)
  ("C-<"  #'mc/mark-previous-like-this)
  ("C-c C-<"  #'mc/mark-all-like-this))

(defun my/choose-language ()
  (interactive "P")
  (let ((choices '("js" "csharp" "clojure" "python" )))
    (message "%s" (completing-read "Choose language: " choices))))

(defun my/flake-setup (a)
  "Choose a language and project name to setup a Nix Flake based
project."
  (interactive "P")
  (let* ((my-lang (my/choose-language))
        (mydir (read-string "Project name: "))
        (newdir (concat "~/source/" my-lang "/" mydir)))
    (mkdir newdir)
    (dired newdir)
    (shell-command "fs")
    (revert-buffer)))

(use-package emacs
  :straight nil
  :general
  ("C-S-<backspace>" #'my/kill-whole-line))

(provide 'a-defaults)
