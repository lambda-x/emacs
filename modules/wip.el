(defun change_inner2 ()
  "Change inner brackets"
  (interactive)
  (sp-backward-up-sexp)
  (set-mark-command nil)
  (forward-char)
  (kill-ring-save (region-beginning) (region-end))
  (backward-char)
  (sp-forward-sexp)
  (set-mark-command nil)
  (backward-char)
  (kill-ring-save (region-beginning) (region-end))
  (forward-char)
  (sp-backward-sexp)
  (sp-mark-sexp)
  (backward-delete-char-untabify 1)
  (yank 2)
  (yank 2)
  (backward-char))

(defun change_inner3 (a)
  "Change inner brackets"
  (interactive "P")
  (let ((s (char-to-string
            (read-char "Input a char:")))
        (o-curl "}")
        (o-brack "]")))
  (if (equal s "{")
      (let ((s o-curl))))
    (search-forward s)
  (backward-char)
  (set-mark-command nil)
  
    (search-backward "{")
    (forward-char)
    (kill-region (region-beginning) (region-end)))

(setq trees '(("pine" . "cones") (oak . acorns) (maple . seeds)))


;; My own code
(setq aa-pairs '(("braces"    ("left" . "{") ("right" . "}"))
                 ("parens"    ("left" . "(") ("right" . ")"))
                 ("brackets"  ("left" . "[") ("right" . "]"))
                 ("angle"     ("left" . "<") ("right" . ">"))))

(setq user-input (char-to-string (read-char "Input a char:")))

(if (equal user-input (or ("}")
                          ("{")))
    (let ((user-i "braces"))))

(defun testy (a)
  (let ((user-input (char-to-string (read-char "Input a char:")))
        (user-i 9999))
    (or (or (equal user-input "}")
            (equal user-input "{"))
        (let ((user-i "braces"))
          
          (or (equal user-input "(")
              (equal user-input ")"))
          (let ((user-i "parens"))

            (or (equal user-input "[")
                (equal user-input "]"))
            (let ((user-i "brackets"))

              (or (equal user-input "<")
                  (equal user-input ">"))
              (let ((user-i "angle"))))))

        user-i)
         ))

(defun test-input (a)
  (setq user-input (char-to-string (read-char "Input a thingy:")))

  (if (or (equal user-input "}")
          (equal user-input "{"))
      (setq user-input "braces"))

  (if (or (equal user-input "]")
          (equal user-input "["))
      (setq user-input "brackets"))

  (if (or (equal user-input "(")
          (equal user-input ")"))
      (setq user-input "parens"))

  (if (or (equal user-input "<")
          (equal user-input ">"))
      (setq user-input "angle"))
 
  user-input)

(test-input nil)




(defun return_assoc (a)
  (setq ui (test-input nil))
  ;; (cdr (car (cdr (assoc ui aa-pairs))))
  (lhs (ui))
  )

(return_assoc nil)

;; "left" value for INPUT
(defun lhs (x)
  (cdr (car (cdr (assoc x aa-pairs)))))
(lhs (test-input nil))

;; "right" value for INPUT
(defun rhs (x)
  (cdr (car (cdr (cdr (assoc x aa-pairs))))))
(rhs (test-input nil))

;; ;; ;; ;;

(defun ci (a)
  (interactive "P")
  (change_inner3 a))

(global-set-key (kbd "s-i") #'change_inner3)

;; This line allows input from user
(char-to-string (read-char "Input a char:"))

()
(use-package change-inner)

(global-set-key (kbd "s-{") (change-inner* "{" nil))


(defun change_inner ()
  "Change inner brackets"
  (interactive)
  (backward-up-list)
  (set-mark-command nil)
  (forward-char)
  (kill-ring-save (region-beginning) (region-end))
  (backward-char)
  (forward-sexp)
  (set-mark-command nil)
  (backward-char)
  (kill-ring-save (region-beginning) (region-end))
  (forward-char)
  (backward-sexp)
  (mark-sexp)
  (backward-delete-char-untabify 1)
  (yank 2)
  (yank 2)
  (backward-char))

()

((

(yank 2)()








