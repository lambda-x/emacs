(use-package lsp-mode
  :hook ((csharp-mode . lsp)
         (rjsx-mode . lsp)
         (web-mode . lsp)
         (scala-mode . lsp)
         (fsharp-mode . lsp)
         (lsp-mode . lsp-lens-mode)
         (lsp-mode . lsp-enable-which-key-integration))
  :custom
  (lsp-keymap-prefix "s-l")
  (lsp-enable-snippet nil)
  (lsp-verify-signature nil)
  (lsp-fsharp-auto-workspace-init t)
  :config
  ;; See this discussion:
  ;; https://github.com/wbolster/emacs-direnv/issues/17
  (advice-add 'lsp :before #'direnv-update-environment)
  
  (setq lsp-enable-which-key-integration t)
  (defhydra hydra-lsp (:hint nil)
"
^Navigation^
------------
Find _d_efinition
Find _r_eferences
"
("d" lsp-find-definition)
("r" lsp-find-references)
("q"   nil "quit" :color blue))
  :bind (:map lsp-mode-map
              ("s-h" . hydra-lsp/body))
  :commands lsp)

(use-package lsp-ui
  ;; :after lsp-mode
  ;; :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-doc-enable t)
  (lsp-ui-doc-use-childframe t)
  (lsp-ui-doc-position 'at-point)
  (lsp-ui-doc-include-signature t)
  (lsp-ui-doc-show-with-cursor nil)
  (lsp-ui-flycheck-enable t)
  (lsp-ui-flycheck-list-position 'bottom)
  (lsp-ui-flycheck-live-reporting t)
  (lsp-ui-sideline-enable t)
  (lsp-ui-sideline-ignore-duplicate t)
  (lsp-ui-sideline-show-symbol t)
  (lsp-enable-symbol-highlighting nil)
  (lsp-ui-sideline-show-hover nil)
  (lsp-ui-sideline-show-diagnostics t)
  (lsp-ui-sideline-show-code-actions t)
  (lsp-ui-sideline-code-actions-prefix " ")
  (lsp-ui-peek-enable t)
  (lsp-ui-peek-list-width 60)
  (lsp-ui-peek-peek-height 25)
  (lsp-ui-imenu-enable t)
  (lsp-ui-imenu-kind-position 'top)
  (lsp-lens-mode t)
  :config (lsp-diagnostics-modeline-mode)
  :commands (lsp-ui-mode)
  :general
  (:keymaps 'lsp-mode-map
            "s-b" #'lsp-ui-doc-glance))

(use-package consult-lsp
  :after (lsp-mode))

(use-package dap-mode
  :commands dap-debug
  :config
  (dap-mode t)
  (dap-ui-mode t))

(use-package helm-lsp
  :after (lsp-mode))

(use-package lsp-ivy
  :after (lsp-mode))

(use-package eglot
  :commands eglot)

(use-package eglot-fsharp
  :after eglot)

(provide 'lang-lsp)
