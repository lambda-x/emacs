(use-package cider
  :mode ("\\.clj\\'"))

(provide 'lang-clojure)
