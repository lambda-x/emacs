(use-package geiser
  :disabled 
  :config
  (setq geiser-active-implementations '(guile)))

(provide 'lang-guile)
