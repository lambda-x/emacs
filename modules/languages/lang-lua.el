(use-package lua-mode
  :mode ("\\.lua\\'"))

(provide 'lang-lua)
