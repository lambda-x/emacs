(use-package scala-mode )

(use-package lsp-metals
  ;; :custom
  ;; (lsp-metals-install-version "latest.snapshot")
  )

(use-package sbt-mode
  :commands sbt-start sbt-command)

(provide 'lang-scala)
