(use-package tuareg
  :mode ("\\.ml\\'"))

(use-package merlin
  :mode ("\\.ml\\'"))

(provide 'lang-ocaml)
