(use-package fsharp-mode
  :mode ("\\.fsharp\\'"))

(provide 'lang-fsharp)
