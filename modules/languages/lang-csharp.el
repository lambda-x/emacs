(use-package csharp-mode
  :mode "\\.cs\\'"
  ;; :config
  ;; (add-hook 'csharp-mode-local-vars-hook #'lsp))
  )

(use-package sharper
  :disabled t)

(provide 'lang-csharp)
