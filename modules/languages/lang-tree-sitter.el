(use-package tree-sitter
  :config
  (require 'tree-sitter-langs)
  (add-hook 'tree-sitter-after-on-hook  #'tree-sitter-hl-mode)
  (add-hook 'js2-mode-hook #'tree-sitter-mode))

(use-package tree-sitter-langs
  :after tree-sitter)

(provide 'lang-tree-sitter)
