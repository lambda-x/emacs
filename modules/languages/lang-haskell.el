(use-package haskell-mode
  ;; :hook interactive-haskell-mode
  :hook (haskell-mode . #'lsp)
  :bind (:map haskell-mode-map
              ("C-c C-l" . haskell-process-load-or-reload)))

(use-package lsp-haskell)

(provide 'lang-haskell)
