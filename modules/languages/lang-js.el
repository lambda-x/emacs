(use-package js-mode
  :straight nil
  ;; :defer 2
  :mode ("\\.js\\'") 
  :init
  (setq js-indent-level 2)
  :commands (js-mode))

(use-package prettier)

(use-package prettier-js
  :hook (js2-mode . prettier-js-mode))

;; (use-package js2-mode
;;   :mode "\\.js\\'")

;; (use-package js2-refactor
;;   :hook js2-mode)

(use-package rjsx-mode
  :mode ("\\.jsx\\'")
  :config
  (setq js2-mode-show-parse-errors nil
        js2-mode-show-strict-warnings nil
        js2-basic-offset 2
        js-indent-level 2)
  (setq-local flycheck-disabled-checkers (cl-union flycheck-disabled-checkers
                                                   '(javascript-jshint)))
  (electric-pair-mode 1))

(use-package indium
  :disabled
  :commands (indium-launch))

(use-package typescript-mode
  :mode "\\.ts\\'"
  :config
  (setq typescript-indent-level 2))

(use-package web-mode
  ;; Unfortunately `web-mode' does not come with `auto-mode-alist'
  ;; autoloads. We have to establish them manually. This list comes
  ;; from the official website at <http://web-mode.org/> as of
  ;; 2018-07-09.
  :mode (("\\.phtml\\'" . web-mode)
         ("\\.tpl\\.php\\'" . web-mode)
         ("\\.[agj]sp\\'" . web-mode)
         ("\\.as[cp]x\\'" . web-mode)
         ("\\.erb\\'" . web-mode)
         ("\\.mustache\\'" . web-mode)
         ("\\.djhtml\\'" . web-mode)
         ("\\.html?\\'" . web-mode)
         ;; My additions.
         ;; ("\\.ejs\\'" . web-mode)
         ("\\.jsx?\\'" . web-mode)
         ("\\.tsx?\\'" . web-mode)
         ("\\.css\\'" . web-mode))
  ;; Use `web-mode' rather than `js-mode' for scripts.
  ;; :interpreter (("js" . web-mode)
  ;;               ("node" . web-mode))
  :config
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-auto-pairs t)
  (setq web-mode-auto-close-style 2)
  (setq web-mode-css-indent-offset 2))

(use-package tide
  :disabled
  :after (typescript-mode company flycheck)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)
         (before-save . tide-format-before-save)))

(provide 'lang-js)
