;;;; -*- lexical-binding: t -*-
(use-package org
  :defer 2
  :mode ("\\.txt\\'") 
  :init
  ;; (add-hook 'org-mode-hook #'org-indent-mode)

;;   (defun skx/update-org-modified-property ()
;;     "If a file contains a '#+LAST_MODIFIED' property update it to contain
;; the current date/time"
;;     (interactive)
;;     (save-excursion
;;       (widen)
;;       (goto-char (point-min))
;;       (when (re-search-forward ":LAST_MODIFIED:" (point-max) t)
;;         (progn
;;           (kill-line)
;;           (insert (format-time-string " %d/%m/%Y %H:%M:%S") )))))

;;   (add-hook 'org-mode-hook 'skx/update-org-modified-property)
  (require 'org-protocol)
  (add-hook 'org-mode-hook #'turn-on-auto-fill)
  (add-hook 'org-mode-hook #'olivetti-mode)
  (add-hook 'org-mode-hook #'variable-pitch-mode)
  :custom
  (org-use-tag-inheritance nil)
  (org-agenda-files '("~/Dropbox/org"))
  ;; (org-return '(indent nil))
  (org-ellipsis " ᵩ")
  (org-startup-folded t)
  (org-special-ctrl-a/e t)
  (org-special-ctrl-k nil)
  (org-return-follows-link t)
  (org-export-with-toc nil)
  (org-id-link-to-org-use-id t)
  :config
  (setq org-latex-pdf-process '("latexmk -lualatex %f"))
  (setq org-insert-heading-respect-content nil)
  (setq org-default-notes-file "~/Dropbox/org/inbox.org")
  (setq org-src-tab-acts-natively nil)
  (set-face-attribute 'org-drawer nil
        :height .6)
  (set-face-attribute 'org-ellipsis nil
        :height 1)
  (require 'org-tempo)

  (setq org-archive-location "~/Dropbox/org/archive.org::")
  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (org-babel-do-load-languages 'org-babel-load-languages
       '(
         (shell . t)
         )
       )
  (setq org-export-with-smart-quotes t)

  :bind (:map org-mode-map
         ("<return>" . evil-org-return))

  :general
  (general-spc org-mode-map
   "'" '(org-edit-special :wk "Edit Special")
   "ft" '(org-babel-tangle :wk "Tangle File")
   "ns" '(org-narrow-to-subtree :wk "Narrow to Subtree")
   "nb" '(org-narrow-to-block :wk "Narrow to Block")
   "cc" '(org-ctrl-c-ctrl-c :wk "Org C-c C-c")
   "." '(org-time-stamp :wk "Time Stamp")
   "!" '(org-time-stamp-inactive :wk "Inactive Time Stamp")
   "p" '(org-set-property :wk "Set Property")
   "s" '(org-schedule :wk "Schedule")
   ":" '(org-set-tags-command :wk "Tags"))

  (general-g org-mode-map
    "c" '(:ignore t :wk "Org")
    "cw" '(org-refile :wk "Refile")
    "c$" '(org-archive-subtree :wk "Archive"))

  (general-spc org-src-mode-map
   "'" '(org-edit-src-exit :wk "Exit")
   "k" '(org-edit-src-abort :wk "Abort"))

  :blackout org-indent-mode
  :blackout auto-fill-mode)

(use-package org-superstar
  :hook (org-mode . org-superstar-mode))

(use-package org-download
  :after org
  :hook (org-mode . org-download-enable))

(use-package evil-org
  :after org
  :hook ((org-mode . evil-org-mode)
         (org-agenda-mode . evil-org-mode)
         (evil-org-mode . (lambda () (evil-org-set-key-theme
                                      '(navigation
                                        todo
                                        insert
                                        textobjects
                                        additional
                                        calendar)))))
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys)
  :blackout)

(use-package org-ref
  :defer t)
