
(defun js-choices ()
  (interactive)
  (let ((choice (completing-read-multiple "Select: " '("node" "typescript" "es_lint"))))
    choice))

(test-main)

(defun fmt-each-choice (list)
    (dolist (choice list)
      (format "%s" choice)))

(dolist (thing '(hello world))
  (format "%s" thing)
  (message thing))

(message "hello")

(defun print-elements-of-list (list)
  "Print each element of LIST on a line of its own."
  (while list
    (print (car list))
    (setq list (cdr list))))

(print-elements-of-list (js-choices))


(fmt-each-choice '(a b c))

(defun reverse (list)
  (let (value)
    (dolist (elt list value)
      (setq value (cons elt value)))))

(defun echothing (list)
  (let (value)
    (dolist (blah list value)
      (message (concat "This will be installed: " blah)))))

(echothing (test-main))

(reverse (test-main))

(defun find-replace-line (a)
    (search-forward "Replace me")
  (back-to-indentation)
  (kill-line)
  (point))

(defun find-end-line (a)
  (search-forward))

(defun my/flake-setup-with-lang (a)
  "Choose a language and project name to setup a Nix Flake based
project."
  (interactive "P")
  (let* ((lang (my/choose-language))
        (mydir (read-string "Project name: "))
        (newdir (concat "~/source/" lang "/" mydir))
        (choices (if (equal lang "js")
                     (js-choices))))
    (mkdir newdir)
    (dired newdir)
    (shell-command "fs")
    (testing3 (concat newdir "/flake.nix") choices)
    (revert-buffer)))


(defun testing (f)
  "replace shit"
  (setq testt ((with-temp-buffer
    (insert-file-contents f)
    (search-forward "# Replace me")
    (replace-match "deno\ntypescript" )
    (buffer-string)))))

(defun testing2 (f choices)
  "replace shit"
  (with-temp-file f
    (insert-file-contents f)
    (search-forward "# Replace me")
    (replace-match choices)
    (buffer-string)))

(defun testing3 (f)
  "replace shit"
  ;; (setq print-these (choices))
  (with-temp-file f
    (insert-file-contents f)
    (search-forward "# Replace me")
    (back-to-indentation)
    (kill-line)
    (insert-flake-choices)
    (kill-whole-line)
    (nix-mode)
    (indent-region (point-min) (point-max))
    ))

(testing3 "~/source/js/TEST/flake.nix")

(defun insert-flake-choices ()
  "GOOD"
  (interactive)
  (let ((myvals (js-choices)))
        (dolist (var myvals)
          (insert (concat var "\n")))))

(testing "~/source/js/TEST/flake.nix")

(js-choices)
        

        
