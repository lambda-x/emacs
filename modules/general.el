;;;; -*- lexical-binding: t -*-
(use-package general
  :hook ((org-capture-mode . evil-normalize-keymaps)
         (org-src-mode . evil-normalize-keymaps))
  :config
  (general-evil-setup t)

  (general-create-definer general-spc
    :states '(normal visual)
    :prefix "SPC")

  (general-create-definer general-g
    :states '(normal visual)
    :prefix "g")

  (general-create-definer general-gc
    :states '(normal visual)
    :prefix "gc")

  (general-create-definer general-global
    :states '(normal visual insert emacs))

  (general-global
   "M-x" 'helm-M-x
   "C-s" 'helm-occur
   "M-p" 'helm-show-kill-ring)

  (general-g
   :states '(normal visual)
   "c" '(:ignore t :wk "Org")
   "cc" '(org-capture :wk "Capture")
   "co" '(org-roam-capture :wk "Roam Capture")
   "s" '(avy-goto-char-timer :wk "Goto Char"))


  (general-spc with-editor-mode-map
    "c" '(with-editor-finish :wk "Finish")
    "k" '(with-editor-cancel :wk "Cancel"))

  (general-spc
    "a" '(org-agenda :wk "Agenda")
    "b" '(:ingore t :wk "Buffers")
    "bb" '(helm-mini :wk "Switch Buffer")
    "bh" '(mark-whole-buffer :wk "Mark Whole Buffer")
    "bk" '(kill-this-buffer :wk "Kill Buffer")
    "bi" '(ibuffer :wk "iBuffer")

    "c" '(:ignore t :wk "Custom")
    "cl" '(org-insert-link :wk "Insert Link") 
    "cL" '(org-store-link :wk "Store Link")

    "f" '(:ignore t :wk "File")
    "ff" '(helm-find-files :wk "Find File")
    "fo" '(org-roam-find-file :wk "Roam Find File")
    "fs" '(save-buffer :wk "Save Buffer")
    "fz" '(fzf :wk "FZF")
    "fd" '(fzf-directory :wk "Fzf Directory")

    "g" '(:ignore t :wk "Git")
    "gs" '(magit-status :wk "Status")

    "h" '(:ignore t :wk "Help")
    "hb" '(describe-bindings :wk "Describe Bindings")
    "hk" '(describe-key :wk "Describe Key")
    "hv" '(describe-variable :wk "Describe Variable")
    "hf" '(describe-function :wk "Describe Function")
    "hm" '(describe-mode :wk "Describe Mode")
    "hl" '(find-library :wk "Find Library")
    "h." '(find-function-at-point :wk "Function at Point")
    "hF" '(find-function :wk "Find Function")
    "ho" '(describe-symbol :wk "Describe Symbol")
    "hP" '(describe-package :wk "Describe Package")

    "i" '(helm-imenu :wk "Imenu")

    "d" '(dired :wk "Dired")

    "n" '(:ignore t :wk "Narrow")
    "nn" '(narrow-to-region :wk "Narrow")
    "nw" '(widen :wk "Widen")

    "p" '(:keymap projectile-command-map :package projectile :wk "Projectile")

    "r" '(:ignore t :wk "Bookmark")
    ;; "rc" '(consult-bookmark :wk "Consult")
    "rd" '(bookmark-delete :wk "Delete")
    "rl" '(helm-bookmarks :wk "List")
    "rm" '(bookmark-set :wk "Save")

    "w" '(:ignore t :wk "Window")
    "wv" '(evil-window-vsplit :wk "Vertical Split")
    "wl" '(evil-window-right :wk "Right")
    "wh" '(evil-window-left :wk "Left")
    "wj" '(evil-window-down :wk "Down")
    "wk" '(evil-window-up :wk "Up")
    "wc" '(evil-window-delete :wk "Close")
    "wo" '(delete-other-windows :wk "Close Other")))
