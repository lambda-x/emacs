(use-package avy-zap
  :disabled
  :bind (("M-z" . avy-zap-to-char-dwim)
         ("M-Z" . avy-zap-up-to-char-dwim)))

(use-package amx
:disabled)

;; (use-package all-the-icons)

;; (use-package all-the-icons-ivy
;;   :after ivy
;;   :init
;;   (add-hook 'after-init-hook 'all-the-icons-ivy-setup)
;;   :config
;;   (setq all-the-icons-ivy-file-commands '(counsel-find-file
;;   counsel-file-jump counsel-recentf counsel-projectile-find-file
;;   counsel-projectile-find-dir)))

;; (use-package all-the-icons-ivy-rich
;;   :after ivy
;;   :init (all-the-icons-ivy-rich-mode 1))

(use-package dashboard
  :disabled
  :config
  (add-to-list 'dashboard-items '(agenda) t)
  (setq show-week-agenda-p t)
  (setq dashboard-items '((recents . 5)
                          (bookmarks . 5)
                          (agenda . 5)))
  (setq dashboard-startup-banner 'official)
  ; (setq dashboard-center-content t)
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title "Welcome to Emacs!")
  (dashboard-setup-startup-hook)
  :blackout t)

(use-package deft
  :disabled
  :after org
  :bind
  ("C-c d" . deft)
  :custom
  (deft-recursive t)
  (deft-use-filter-string-for-filename t)
  (deft-default-extension "org")
  (deft-directory "~/org-roam"))

(use-package elisp-benchmarks
  :disabled)

(use-package general
  :disabled
  ;; :hook ((org-capture-mode . evil-normalize-keymaps)
  ;;        (org-src-mode . evil-normalize-keymaps))
  :config
  (general-evil-setup t)

  (general-create-definer general-spc
    :states '(normal visual)
    :prefix "SPC")

  (general-create-definer general-g
    :states '(normal visual)
    :prefix "g")

  (general-create-definer general-slash
    :states '(normal visual)
    :prefix "\\")


  (general-create-definer general-gc
    :states '(normal visual)
    :prefix "gc")

  (general-create-definer general-global
    :states '(normal visual insert emacs))

  ;; (general-global
  ;;  "M-x" 'helm-M-x
  ;;  "C-s" 'helm-occur
  ;;  "M-p" 'helm-show-kill-ring)

  (general-g
   :states '(normal visual)
   "c" '(:ignore t :wk "Org")
   "cc" '(org-capture :wk "Capture")
   "co" '(org-roam-capture :wk "Roam Capture")
   "s" '(avy-goto-char-timer :wk "Goto Char"))


  (general-spc with-editor-mode-map
    "c" '(with-editor-finish :wk "Finish")
    "k" '(with-editor-cancel :wk "Cancel"))

  (general-spc
    "a" '(org-agenda :wk "Agenda")
    "b" '(:ingore t :wk "Buffers")
    "bb" '(switch-to-buffer :wk "Switch Buffer")
    "bh" '(mark-whole-buffer :wk "Mark Whole Buffer")
    "bk" '(kill-this-buffer :wk "Kill Buffer")
    "bi" '(ibuffer :wk "iBuffer")

    "c" '(:ignore t :wk "Custom")
    "cl" '(org-insert-link :wk "Insert Link") 
    "cL" '(org-store-link :wk "Store Link")

    "f" '(:ignore t :wk "File")
    "ff" '(find-file :wk "Find File")
    "fo" '(org-roam-find-file :wk "Roam Find File")
    "fs" '(save-buffer :wk "Save Buffer")
    "fz" '(fzf :wk "FZF")
    "fd" '(fzf-directory :wk "Fzf Directory")

    "g" '(:ignore t :wk "Git")
    "gs" '(magit-status :wk "Status")

    "h" '(:ignore t :wk "Help")
    "hb" '(describe-bindings :wk "Describe Bindings")
    "hk" '(describe-key :wk "Describe Key")
    "hv" '(describe-variable :wk "Describe Variable")
    "hf" '(describe-function :wk "Describe Function")
    "hm" '(describe-mode :wk "Describe Mode")
    "hl" '(find-library :wk "Find Library")
    "h." '(find-function-at-point :wk "Function at Point")
    "hF" '(find-function :wk "Find Function")
    "ho" '(describe-symbol :wk "Describe Symbol")
    "hP" '(describe-package :wk "Describe Package")

    "i" '(imenu :wk "Imenu")

    "d" '(dired :wk "Dired")

    "n" '(:ignore t :wk "Narrow")
    "nn" '(narrow-to-region :wk "Narrow")
    "nw" '(widen :wk "Widen")

    "p" '(:keymap projectile-command-map :package projectile :wk "Projectile")

    "r" '(:ignore t :wk "Bookmark")
    ;; "rc" '(consult-bookmark :wk "Consult")
    "rd" '(bookmark-delete :wk "Delete")
    "rl" '(list-bookmarks :wk "List")
    "rm" '(bookmark-set :wk "Save")

    "w" '(:ignore t :wk "Window")
    "wv" '(evil-window-vsplit :wk "Vertical Split")
    "wl" '(evil-window-right :wk "Right")
    "wh" '(evil-window-left :wk "Left")
    "wj" '(evil-window-down :wk "Down")
    "wk" '(evil-window-up :wk "Up")
    "wc" '(evil-window-delete :wk "Close")
    "wo" '(delete-other-windows :wk "Close Other")))

(use-package imenu-list
  :disabled
  :after imenu)

(use-package maple-minibuffer
  :disabled
  :hook (after-init . maple-minibuffer-mode)
  :config
  (setq maple-minibuffer:position-type 'window-bottom-left
        maple-minibuffer:border-color "gray50"
        maple-minibuffer:height nil
        maple-minibuffer:width 0.7
        maple-minibuffer:cache t)

  (setq maple-minibuffer:action '(read-from-minibuffer read-string)
        maple-minibuffer:ignore-action '(evil-ex eval-expression))

  (add-to-list 'maple-minibuffer:ignore-action 'org-schedule)
  (add-to-list 'maple-minibuffer:ignore-regexp "^helm-")

  ;; more custom parameters for frame
  (defun maple-minibuffer:parameters ()
    "Maple minibuffer parameters."
    `((height . ,(or maple-minibuffer:height 10))
      (width . ,(or maple-minibuffer:width (window-pixel-width)))
      (left-fringe . 5)
      (right-fringe . 5))))

(use-package org-bullets
  :disabled
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(use-package org-super-agenda
  :disabled
  :after org
  :config
  (setq org-super-agenda-header-map (make-sparse-keymap))
  (setq org-super-agenda-groups '(
                                  (:name "Today"
                                         :time-grid t
                                         :scheduled today)
                                  (:name "Due Today"
                                         :deadline today)
                                  (:name "Overdue"
                                         :deadline past)
                                  (:name "Due Soon"
                                         :deadline future)))
  (org-super-agenda-mode))

(use-package page-break-lines
  :disabled
  :blackout t)

(use-package yasnippet
  :config
  (yas-reload-all)
  :commands (yas-minor-mode yas-global-mode))

(use-package yasnippet-snippets
  :after yasnippet)
