;;;; -*- lexical-binding: t -*-
(use-package mu4e
  :straight nil
  :load-path "/Users/anthony/.nix-profile/share/emacs/site-lisp/mu4e"
  :commands (mu4e)
  :config
  (setq mu4e-index-update-error-warning nil)
  ;; (setq smtpmail-smtp-server "smtp.gmail.com"
  ;;       smtpmail-smtp-service 587
  ;;       smtpmail-stream-type 'starttls)

  (setq mu4e-contexts
        (list

         (make-mu4e-context
          :name "gmail"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/gmail" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "anthony.c.adrian@gmail.com")
                  (mu4e-drafts-folder . "/gmail/Drafts")
                  (mu4e-sent-folder . "/gmail/Sent")
                  (mu4e-refile-folder . "/gmail/All Mail")
                  (mu4e-trash-folder . "/gmail/Trash")
                  (smtpmail-smtp-server . "smtp.gmail.com")
                  (smtpmail-smtp-service . 587)
                  (smtpmail-stream-type . starttls)))

         (make-mu4e-context
          :name "outlook"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/outlook" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "anthony.adrian@outlook.com")
                  (mu4e-drafts-folder . "/outlook/Drafts")
                  (mu4e-sent-folder . "/outlook/Sent")
                  (mu4e-refile-folder . "/outlook/Archive")
                  (mu4e-trash-folder . "/outlook/Deleted")
                  (smtpmail-smtp-server . "smtp.outlook.com")
                  (smtpmail-smtp-service . 587)
                  (smtpmail-stream-type . starttls)))))

  ;; (setq user-mail-address "anthony.adrian@outlook.com"
  ;;       smtpmail-smtp-server "smtp.outlook.com"
  ;;       smtpmail-smtp-service 587
  ;;       smtpmail-stream-type 'starttls)

  (setq message-send-mail-function 'smtpmail-send-it)
  (setq mu4e-change-filenames-when-moving t)
  (setq mu4e-update-interval (* 10 60))
  (setq mu4e-get-mail-command "mbsync -a")
  (setq mu4e-maildir "~/Mail")

  ;; (setq mu4e-drafts-folder "/Drafts")
  ;; (setq mu4e-sent-folder   "/Sent")
  ;; (setq mu4e-refile-folder "/Archive")
  ;; (setq mu4e-trash-folder  "/Deleted")
  )

(provide 'a-mu4e)
