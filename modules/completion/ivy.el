(use-package ivy
  :disabled
  :demand t
  :bind (("C-x b" . ivy-switch-buffer)
         ("C-x B" . ivy-switch-buffer-other-window)
         ("M-H"   . ivy-resume))
  :bind (:map ivy-switch-buffer-map
              ("C-k" . ivy-switch-buffer-kill))
  :bind (:map ivy-minibuffer-map
              ("<tab>" . ivy-alt-done))
  :custom
  (ivy-initial-inputs-alist nil t)
  (ivy-use-virtual-buffers nil)
  (ivy-use-selectable-prompt t)
  :config
  (ivy-mode 1)
  :blackout t)

(use-package ivy-rich
  :disabled
  :after counsel
  ;; :straight t
  :init (ivy-rich-mode 1))

(use-package counsel
  :disabled
  :after ivy
  ;; :demand t
  :bind
  ("M-x" . 'counsel-M-x)
  ("C-x C-f" . 'counsel-find-file)
  ("C-h f" . 'counsel-describe-function)
  ("C-h v" . 'counsel-describe-variable)
  ("C-h o" . 'counsel-describe-symbol)
  ("C-h l" . 'counsel-find-library)
  ("C-x r l" . 'counsel-bookmark)
  :config
  (ivy-mode 1)
  (setq enable-recursive-minibuffers t)
  (minibuffer-depth-indicate-mode 1)
  (setq ivy-count-format "(%d/%d) ")
  :commands (counsel-load-theme))

;; (aa/leader-key-def
;;   "r"   '(ivy-resume :which-key "ivy resume")
;;   "f"   '(:ignore t :which-key "files")
;;   "ff"  '(counsel-find-file :which-key "open file"))

(use-package swiper
  :disabled
  :after ivy
  :bind ("C-M-s" . swiper)
  :bind (:map swiper-map
              ("M-y" . yank)
              ("M-%" . swiper-query-replace)
              ("C-." . swiper-avy)
              ("M-c" . swiper-mc))
  :bind (:map isearch-mode-map
              ("C-o" . swiper-from-isearch)))
