(use-package selectrum
  :disabled
  :demand
  :init
  (selectrum-mode +1))

(use-package prescient
  :disabled
  :init
  (setq prescient-history-length 1000)
  :config
  ;; Remember usage statistics across Emacs sessions.
  (prescient-persist-mode +1)
  ;; The default settings seem a little forgetful to me. Let's try
  ;; this out.
  )

(use-package selectrum-prescient
  :disabled
  :demand
  :after selectrum
  :config
  (selectrum-prescient-mode +1))

(use-package ivy-prescient
  :disabled
  :after counsel
  :config
  (ivy-prescient-mode +1))
