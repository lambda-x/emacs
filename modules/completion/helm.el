(use-package helm
  :custom
  (helm-M-x-fuzzy-match t)
  (helm-buffers-fuzzy-matching t)
  (helm-allow-mouse t)
  (setq helm-split-window-in-side-p t)
  :commands (helm-mode))

(use-package helm-rg
  :commands (helm-rg))
