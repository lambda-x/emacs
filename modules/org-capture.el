(use-package org-capture
    :ensure nil
    :after org
    :hook (org-capture-mode . evil-insert-state)
    :config
    ;; (setq org-modules (quote (org-protocol)))
    (setq org-refile-targets '((nil :maxlevel . 9)
                                  (org-agenda-files :maxlevel . 9)))
    ;; (setq org-refile-tagets '(("bookmarks.org" :maxlevel . 2)))
    (setq org-capture-templates
          '(
            ("n" "Note" entry
             (file "~/Dropbox/org/inbox.org")
             "* NOTE %?
:PROPERTIES:
:ID: %(shell-command-to-string \"uuidgen\"):CREATED: %U\n:DESCRIPTION:
:END:" :prepend t)
            ("t" "Work related task" entry
             (file "~/Dropbox/org/inbox.org")
             "* TODO %^{Description}\n:PROPERTIES:\n:ID: %(shell-command-to-string \"uuidgen\"):CREATED: %U\n:END:\n%?" :prepend t)
            ("b" "Bookmarks" entry
             (file "~/Dropbox/org/inbox.org")
             "*** %?\n:PROPERTIES:\n:Added: %u\n:END:\n%?\n" :prepend t)
            ("j" "Journal" entry
             (file+headline "~/Dropbox/org/journal.org" "Days")
             "** %u\n \n %?" :prepend t)
             ("y" "Web site" entry
             (file "~/Dropbox/org/inbox.org")
  "* %a :website:\n:PROPERTIES:\n:CREATED: %U\n:ID: %(shell-command-to-string \"uuidgen\"):END:\n\n %?\n\n%:initial" :prepend t)
            ("w" "Words" entry
             (file+headline "~/Dropbox/org/journal.org" "Words")
             "** %?\n:PROPERTIES:\n:Added: %u\n:END:\n%i\n" :prepend t)
            ))
    :bind ("C-c c" . 'org-capture)
    :general
    (general-spc org-capture-mode-map
    "c" '(org-capture-finalize :wk "Finalize")
    "w" '(org-capture-refile :wk "Refile")
    "k" '(org-capture-kill :wk "Abort")))
