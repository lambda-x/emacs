(use-package org
  :init
  ;; (require 'org-protocol)
  (add-hook 'org-mode-hook #'turn-on-auto-fill)
  (add-hook 'org-mode-hook #'olivetti-mode)
  ;; (add-hook 'org-mode-hook #'variable-pitch-mode)
  :custom
  ;; Prevent subheadings from inheriting tags. This makes it easier to
  ;; sift through sparse tree results as it cuts down the number of
  ;; qualifying headings.
  (org-use-tag-inheritance nil)
  (org-agenda-files '("~/Dropbox/org/agenda/"))
  (org-directory "~/Dropbox/org/agenda/")
  ;; (org-return '(indent nil))
  (org-ellipsis " ᵩ")
  (org-support-shift-select t)
  (org-startup-folded t)
  (setq org-startup-options 'show2levels)
  (org-special-ctrl-a/e t)
  (org-special-ctrl-k nil)
  (org-return-follows-link t)
  (org-use-speed-commands t)
  ;; Prevent LaTeX and HTML exports from generating tocs. If needed,
  ;; the toc can be added on a per file basis.
  (org-export-with-toc nil)
  ;; Generate UUIDs for headings to serve as link IDs.
  (org-id-link-to-org-use-id t)
  :config
  (setq org-latex-pdf-process '("latexmk -lualatex %f"))
  (setq org-insert-heading-respect-content nil)
  (setq org-default-notes-file "~/Dropbox/org/inbox.org")
  (setq org-src-tab-acts-natively nil)
  (setq org-todo-keywords
  '((sequence "TODO" "NOTES" "|" "DONE")))
  (set-face-attribute 'org-drawer nil
        :height .6)
  (set-face-attribute 'org-ellipsis nil
        :height 1)
  (require 'org-tempo)

  (require 'org-protocol)

  (setq org-archive-location "~/Dropbox/org/archive.org::")
  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("js" . "src javascript"))
  (add-to-list 'org-structure-template-alist '("html" . "src html"))
  (org-babel-do-load-languages 'org-babel-load-languages
                               '((clojure . t)
                                 (css . t)
                                 (haskell . t)
                                 (lisp . t)
                                 (python . t)
                                 (shell . t)
                                 (js . t)))
  
  (setq org-export-with-smart-quotes t)
  :hook (org-mode . org-indent-mode)
  :blackout org-indent-mode
  :blackout auto-fill-mode
  :commands (org-mode))

(use-package org-tempo
  :straight nil
  :after org)

(use-package org
  :straight nil
  :after org
  :config
  (defhydra hydra-org (:hint nil)
  "
  ^Navigation^            ^Structure Editing^
  -----------------------------------------
  _n_ext heading          Promote _U_p
  _p_revious heading      Demote _D_own
  _f_orward same level    meta-_l_eft
  _b_ackward same level   meta-_r_ight
  _u_p level              shiftmeta_R_ight
  <TAB> org-cycl           shiftmeta_L_eft
  "
  ("p"   org-previous-visible-heading)
  ("n"   org-next-visible-heading)
  ("f"   org-forward-heading-same-level)
  ("b"   org-backward-heading-same-level)
  ("u"   outline-up-heading)
  ("F"   org-next-block)
  ("B"   org-previous-block)
  ("r"   org-metaright)
  ("l"   org-metaleft)
  ("R"   org-shiftmetaright)
  ("L"   org-shiftmetaleft)
  ("U"   org-metaup)
  ("D"   org-metadown)
  ("w"   org-refile)
  ("q"   nil "quit" :color blue)
  ("[?\\t" org-cycle))
  :bind (:map org-mode-map
  ("s-f" . hydra-org/body)))

(use-package org-bindings
  :straight nil
  :bind (:map org-mode-map
              ("<return>" . org-return)
              ("M-n" . org--forward-paragraph-once)
              ("M-p" . org--backward-paragraph-once)))

(use-package org
  :config
  (setq org-refile-targets '((nil :maxlevel . 9)
                             (org-agenda-files :maxlevel . 9)))
  (add-hook 'org-capture-prepare-finalize-hook 'org-id-get-create) ;; found here: https://stackoverflow.com/questions/13340616/assign-ids-to-every-entry-in-org-mode
  :bind ("C-c c" . 'org-capture))
(setq org-capture-templates
      `(("n" "Note" entry
         (file "~/Dropbox/org/agenda/inbox.org")
         ,(concat "* NOTE %^{Title}\n"
                  ":PROPERTIES:\n"
                  ":ID: %(shell-command-to-string \"uuidgen\")"
                  ":CREATED: %U\n"
                  ":DESCRIPTION:\n"
                  ":END:\n"
                  "\n%?")
         :prepend t)
        ("p" "Passage" entry
         (file+headline "inbox.org" "Quotes")
         ,(concat "* %^{Heading}\n"
                  ":PROPERTIES:\n"
                  ":Author: %^{Author}\n"
                  ":Title: %^{Title}\n"
                  ":Pages: %^{Pages}\n"
                  ":END:\n\n"
                  "#+begin_quote\n\n %?\n\n #+end_quote")
        ("j" "Journal Entry" entry
         (file+datetree "new_journal.org")
         ,(concat "* %(format-time-string \"%H:%M\") - %?\n"
                  ":PROPERTIES:\n"
                  ":CAPTURED: %U\n"
                  ":END:\n\n"
                  "%i\n  %a")))
        ("t" "Todo Task" entry
         (file "~/Dropbox/org/agenda/inbox.org")
         ,(concat "* TODO %^{Description}\n"
                  ":PROPERTIES:\n"
                  ":ID: %(shell-command-to-string \"uuidgen\")"
                  ":CREATED: %U\n"
                  ":END:\n"
                  "\n%?")
         :prepend t)
        ("w" "Words" entry
         (file+headline "~/Dropbox/org/journal.org" "Words")
         ,(concat "* %^{Word}\n"
                  ":PROPERTIES:\n"
                  ":Added: %u\n"
                  ":END:\n"
                  "\n%^{Part of Speech|adjective|noun|verb|adverb}\n"
                  "\n%?")
         :prepend t)
        ("y" "Website" entry
         (file "~/Dropbox/org/inbox.org")
         ,(concat "* %a %^g\n"
                  ":PROPERTIES:\n"
                  ":CREATED: %U\n"
                  ":ID: %(shell-command-to-string \"uuidgen\")"
                  ":END:\n"
                  "\n#+begin_quote\n"
                  "%i\n"
                  "#+end_quote\n")
         :prepend t)))

(use-package org-ql)

;; (org-ql-query
;;   :select #'org-get-heading
;;   :from "~/Dropbox/org/agenda/inbox.org"
;;   :where '(todo))

;; (org-ql-select "~/Dropbox/org/agenda/inbox.org"
;;   '(todo)
;;   :action #'org-get-heading)

;; (org-ql-search (org-agenda-files)
;;   '(or (and (not (done))
;;             (or (habit)
;;                 (deadline auto)
;;                 (scheduled :to today)
;;                 (ts-active :on today)))
;;        (closed :on today))
;;   :sort '(todo priority date))

;; (org-ql-search "~/Dropbox/org/agenda/inbox.org"
;;   '(and (property "Author" "Ned Block"))
;;   )

;; (org-ql-query
;;   :select #'org-get-heading
;;   :from "~/Dropbox/org/agenda/inbox.org"
;;   :where '(property "Author" "Ned Block"))

(use-package org-agenda
  :straight nil
  :after org
  :config
  (setq org-agenda-confirm-kill t)
  :bind (("C-c a" . org-agenda)))

(use-package org-superstar
  :hook (org-mode . org-superstar-mode))

(use-package org-download
  :after org
  :hook (org-mode . org-download-enable))

(use-package org-ref
  :defer t
  :commands (org-ref-insert-link))

(use-package org-journal
  :commands (org-journal-mode))

(use-package ox-hugo
  :after ox
  :config
  (setq org-hugo-base-dir "~/source/website/"))

(defvar home-dir "/Users/anthony")
(defvar latex-dir (concat home-dir "/dots/config/latex"))

 (add-to-list 'org-latex-classes
              '("memoir"
              "\\input{~/dots/config/latex/memoir.tex}
               [NO-DEFAULT-PACKAGES]
               [PACKAGES]
               [EXTRA]"
                ("\\part{%s}" . "\\part*{%s}")
                ("\\chapter{%s}" . "\\chapter*{%s}")
                ("\\section{%s}" . "\\section*{%s}")
                ("\\subsection{%s}" . "\\subsection*{%s}")
                ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                ("\\paragraph{%s}" . "\\paragraph*{%s}")
                ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

 (add-to-list 'org-latex-classes
              '("two-column"
                "\\input{/Users/anthony/dots/latex/article_2.tex}
             [NO-DEFAULT-PACKAGES]
             [PACKAGES]
             [EXTRA]"
                ("\\part{%s}" . "\\part*{%s}")
                ("\\chapter{%s}" . "\\chapter*{%s}")
                ("\\section{%s}" . "\\section*{%s}")
                ("\\subsection{%s}" . "\\subsection*{%s}")
                ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                ("\\paragraph{%s}" . "\\paragraph*{%s}")
                ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))


 (add-to-list 'org-latex-classes
              '("book"
                "\\input{/Users/anthony/dots/config/latex/pp.tex}"
                ("\\part{%s}" . "\\part*{%s}")
                ("\\chapter{%s}" . "\\chapter*{%s}")
                ("\\section{%s}" . "\\section*{%s}")
                ("\\subsection{%s}" . "\\subsection*{%s}")
                ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                ("\\paragraph{%s}" . "\\paragraph*{%s}")
                ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

 (add-to-list 'org-latex-classes
              '("blank"
                "[NO-DEFAULT-PACKAGES]
                [NO-PACKAGES]
                [EXTRA]"
                ("\\section{%s}" . "\\section*{%s}")
                ("\\subsection{%s}" . "\\subsection*{%s}")
                ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                ("\\paragraph{%s}" . "\\paragraph*{%s}")
                ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))






(add-to-list 'org-latex-classes
             '("ethz"
               "\\documentclass[a4paper,11pt,titlepage]{memoir}
\\usepackage[utf8]{inputenc}
\\usepackage[T1]{fontenc}
\\usepackage{fixltx2e}
\\usepackage{graphicx}
\\usepackage{longtable}
\\usepackage{float}
\\usepackage{wrapfig}
\\usepackage{rotating}
\\usepackage[normalem]{ulem}
\\usepackage{amsmath}
\\usepackage{textcomp}
\\usepackage{marvosym}
\\usepackage{wasysym}
\\usepackage{amssymb}
\\usepackage{hyperref}
\\usepackage{mathpazo}
\\usepackage{color}
\\usepackage{enumerate}
\\definecolor{bg}{rgb}{0.95,0.95,0.95}
\\tolerance=1000
      [NO-DEFAULT-PACKAGES]
      [PACKAGES]
      [EXTRA]
\\linespread{1.1}
\\hypersetup{pdfborder=0 0 0}"
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))


(add-to-list 'org-latex-classes
             '("article"
               "\\documentclass[11pt,a4paper]{article}
\\usepackage[utf8]{inputenc}
\\usepackage[T1]{fontenc}
\\usepackage{fixltx2e}
\\usepackage{graphicx}
\\usepackage{longtable}
\\usepackage{float}
\\usepackage{wrapfig}
\\usepackage{rotating}
\\usepackage[normalem]{ulem}
\\usepackage{amsmath}
\\usepackage{textcomp}
\\usepackage{marvosym}
\\usepackage{wasysym}
\\usepackage{amssymb}
\\usepackage{hyperref}
\\usepackage{mathpazo}
\\usepackage{color}
\\usepackage{enumerate}
\\definecolor{bg}{rgb}{0.95,0.95,0.95}
\\tolerance=1000
      [NO-DEFAULT-PACKAGES]
      [PACKAGES]
      [EXTRA]
\\linespread{1.1}
\\hypersetup{pdfborder=0 0 0}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")))

(provide 'a-org)
